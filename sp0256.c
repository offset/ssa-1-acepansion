/*
** Converted to plain C without MESS dependencies
** Reworked audio output handling
** Integrated default ROM
** Fixed to handle non bit-flipped custom ROM
** Fixed LRQ timing according to http://www.cpcwiki.eu/index.php/SP0256_Measured_Timings
** by Philippe 'OffseT' Rimauro
*/

/*
   GI SP0256 Narrator Speech Processor
   GI SPB640 Speech Buffer

   By Joe Zbiciak. Ported to MESS by tim lindner.

   Unimplemented:
   - Microsequencer repeat count of zero
   - SPB-640 perpherial/RAM bus

 Copyright Joseph Zbiciak, all rights reserved.
 Copyright tim lindner, all rights reserved.

 - This source code is released as freeware for non-commercial purposes.
 - You are free to use and redistribute this code in modified or
   unmodified form, provided you list us in the credits.
 - If you modify this source code, you must add a notice to each
   modified source file that it has been changed.  If you're a nice
   person, you will clearly mark each change too.  :)
 - If you wish to use this for commercial purposes, please contact us at
   intvnut@gmail.com (Joseph Zbiciak), tlindner@macmess.org (tim lindner)
 - This entire notice must remain in the source code.

 Note: Bit flipping.
    This emulation flips the bits on every byte of the memory map during
    the sp0256_start() call.

    If the memory map contents is modified during execution (accross of ROM
    bank switching) the bitrevbuff() call must be called after the section
    of ROM is modified.
*/

#include <clib/debug_protos.h>
#include <proto/exec.h>
#include <string.h>

#include "sp0256.h"

extern struct Library *SysBase;


#define PER_PAUSE    (64)               /* Equiv timing period for pauses.  */
#define PER_NOISE    (64)               /* Equiv timing period for noise.   */

#define FIFO_ADDR    (0x1800 << 3)      /* SP0256 address of SPB260 speech FIFO.   */

#define VERBOSE 0
#define DEBUG_FIFO 0

#ifdef __HAIKU__
#define LOG(x)
#define LOG_FIFO(x)
#else
#define LOG(x)  do { if (VERBOSE) kprintf x; } while (0)

#define LOG_FIFO(x) do { if (DEBUG_FIFO) kprintf x; } while (0)
#endif


/* ---------------------------------------------------------------- */
/*  Data structure for 12-pole filter.                              */
/* ---------------------------------------------------------------- */
struct lpc12_t
{
    int     rpt, cnt;       /* Repeat counter, Period down-counter.         */
    ULONG   per, rng;       /* Period, Amplitude, Random Number Generator   */
    int     amp;
    SHORT   f_coef[6];      /* F0 through F5.                               */
    SHORT   b_coef[6];      /* B0 through B5.                               */
    SHORT   z_data[6][2];   /* Time-delay data for the filter stages.       */
    UBYTE   r[16];          /* The encoded register set.                    */
    int     interp;

    SHORT   out;            /* 16-bit audio output current status           */
};

/* ---------------------------------------------------------------- */
/*  Data structure for SP0256 emulation.                            */
/* ---------------------------------------------------------------- */
struct sp0256_t
{
    UBYTE         *rom;             /* ROM (64K max).                               */
    USHORT         size;            /* Size of the ROM (power of 2 only).           */

    int            sby_line;        /* Standby line state                           */
    int            cur_len;         /* Fullness of current sound buffer.            */

    int            silent;          /* Flag: SP0256 is silent.                      */

    struct lpc12_t filt;            /* 12-pole filter                               */
    int            lrq;             /* Load ReQuest.  == 0 if we can accept a load  */
    int            ald;             /* Address LoaD.  < 0 if no command pending.    */
    int            pc;              /* Microcontroller's PC value.                  */
    int            stack;           /* Microcontroller's PC stack.                  */
    int            fifo_sel;        /* True when executing from FIFO.               */
    int            halted;          /* True when CPU is halted.                     */
    ULONG          mode;            /* Mode register.                               */
    ULONG          page;            /* Page set by SETPAGE                          */

    ULONG          fifo_head;       /* FIFO head pointer (where new data goes).     */
    ULONG          fifo_tail;       /* FIFO tail pointer (where data comes from).   */
    ULONG          fifo_bitp;       /* FIFO bit-pointer (for partial decles).       */
    USHORT         fifo[64];        /* The 64-decle FIFO.                           */

    int            tick;
    int            tick_ref;
};


/* ======================================================================== */
/*  qtbl  -- Coefficient Quantization Table.  This comes from a             */
/*              SP0250 data sheet, and should be correct for SP0256.        */
/* ======================================================================== */
static const SHORT qtbl[128] =
{
    0,      9,      17,     25,     33,     41,     49,     57,
    65,     73,     81,     89,     97,     105,    113,    121,
    129,    137,    145,    153,    161,    169,    177,    185,
    193,    201,    209,    217,    225,    233,    241,    249,
    257,    265,    273,    281,    289,    297,    301,    305,
    309,    313,    317,    321,    325,    329,    333,    337,
    341,    345,    349,    353,    357,    361,    365,    369,
    373,    377,    381,    385,    389,    393,    397,    401,
    405,    409,    413,    417,    421,    425,    427,    429,
    431,    433,    435,    437,    439,    441,    443,    445,
    447,    449,    451,    453,    455,    457,    459,    461,
    463,    465,    467,    469,    471,    473,    475,    477,
    479,    481,    482,    483,    484,    485,    486,    487,
    488,    489,    490,    491,    492,    493,    494,    495,
    496,    497,    498,    499,    500,    501,    502,    503,
    504,    505,    506,    507,    508,    509,    510,    511
};

/* ======================================================================== */
/*  sp0256_al2_rom  -- Default 2K english ROM.                              */
/*                     It comes from:                                       */
/*                     http://spatula-city.org/~im14u2c/sp0256-al2/al2.bin  */
/*                     It is (c) MicroChip as stated here:                  */
/*                     http://spatula-city.org/~im14u2c/sp0256-al2          */
/* ======================================================================== */
const UBYTE sp0256_al2_rom[] =
{
	0x07, 0xDE, 0x07, 0xE0, 0x07, 0xE2, 0x07, 0xE4, 0x07, 0xE6, 0x07, 0xE9, 0x17, 0x14, 0x17, 0x3F,
	0x17, 0x4C, 0x17, 0x5F, 0x17, 0x72, 0x17, 0x91, 0x17, 0xAD, 0x17, 0xBA, 0x17, 0xD2, 0x17, 0xEF,
	0x17, 0xFC, 0x27, 0x10, 0x27, 0x23, 0x27, 0x3B, 0x27, 0x77, 0x27, 0x9A, 0x27, 0xAB, 0x27, 0xBF,
	0x27, 0xCC, 0x27, 0xDF, 0x37, 0x15, 0x37, 0x22, 0x37, 0x3B, 0x37, 0x53, 0x37, 0x5D, 0x37, 0x6A,
	0x37, 0x89, 0x37, 0xA3, 0x37, 0xB9, 0x37, 0xCF, 0x37, 0xF1, 0x47, 0x07, 0x47, 0x27, 0x47, 0x3B,
	0x47, 0x5A, 0x47, 0x64, 0x47, 0x75, 0x47, 0x8F, 0x47, 0xAE, 0x47, 0xC6, 0x47, 0xDA, 0x47, 0xFC,
	0x57, 0x31, 0x57, 0x58, 0x57, 0x7C, 0x57, 0x8F, 0x57, 0xDE, 0x67, 0x35, 0x67, 0x50, 0x67, 0x68,
	0x67, 0x72, 0x67, 0xA8, 0x67, 0xBD, 0x67, 0xE5, 0x77, 0x3B, 0x77, 0x60, 0x77, 0x76, 0x77, 0x98,
	0x87, 0x00, 0x87, 0x02, 0x87, 0x04, 0x87, 0x06, 0x87, 0x08, 0x87, 0x0A, 0x87, 0x0C, 0x87, 0x0E,
	0x87, 0x10, 0x87, 0x12, 0x87, 0x14, 0x87, 0x16, 0x87, 0x18, 0x87, 0x1A, 0x87, 0x1C, 0x87, 0x1E,
	0x87, 0x20, 0x87, 0x22, 0x87, 0x24, 0x87, 0x26, 0x87, 0x28, 0x87, 0x2A, 0x87, 0x2C, 0x87, 0x2E,
	0x87, 0x30, 0x87, 0x32, 0x87, 0x34, 0x87, 0x36, 0x87, 0x38, 0x87, 0x3A, 0x87, 0x3C, 0x87, 0x3E,
	0x10, 0x00, 0x20, 0x00, 0x30, 0x00, 0x40, 0x00, 0x50, 0x00, 0x60, 0x00, 0x70, 0x00, 0x80, 0x00,
	0x90, 0x00, 0xA0, 0x00, 0xB0, 0x00, 0xC0, 0x00, 0xD0, 0x00, 0xE0, 0x00, 0xF0, 0x00, 0x8F, 0x00,
	0x2F, 0x00, 0xEF, 0x00, 0xFF, 0x00, 0xB8, 0xFF, 0x00, 0x08, 0xCC, 0xA7, 0x69, 0x95, 0xF5, 0xFC,
	0xC2, 0x0D, 0x26, 0x53, 0xC5, 0x6F, 0xE2, 0xAA, 0x2D, 0x7F, 0x94, 0x71, 0x5B, 0xF8, 0xEE, 0xB6,
	0x8A, 0xAE, 0x2F, 0x7E, 0x95, 0xCD, 0x47, 0xF2, 0xAB, 0x6A, 0xBF, 0xA5, 0x5B, 0x53, 0xFE, 0x68,
	0x92, 0xDF, 0xE0, 0x00, 0x08, 0x8C, 0x77, 0x6B, 0xB7, 0xCD, 0xFD, 0x58, 0x45, 0xE4, 0x55, 0xB3,
	0x6F, 0xD3, 0x9D, 0xDA, 0x4A, 0xB5, 0xB3, 0xFA, 0x51, 0xB3, 0xFF, 0x52, 0xAD, 0x6A, 0xFF, 0x95,
	0xEB, 0x7E, 0x78, 0xA7, 0x6A, 0x7F, 0xE5, 0x5A, 0x5B, 0x81, 0x28, 0x92, 0xBC, 0x00, 0x00, 0x18,
	0x6C, 0xDF, 0x6A, 0x82, 0xD2, 0x89, 0x1F, 0x34, 0xB9, 0x32, 0xA8, 0x00, 0x2F, 0x18, 0xC4, 0xB0,
	0x00, 0x5C, 0x41, 0xF8, 0xB6, 0x9D, 0x21, 0x80, 0x18, 0x20, 0x21, 0x11, 0xA8, 0xC0, 0x00, 0xBF,
	0x18, 0x24, 0xA0, 0x00, 0x54, 0x69, 0x7E, 0xE7, 0xEB, 0x21, 0x80, 0x0A, 0xA2, 0x27, 0xD7, 0x3C,
	0xC0, 0x00, 0x18, 0x24, 0xA8, 0x00, 0x94, 0x84, 0xC0, 0x62, 0xF9, 0x67, 0x3B, 0x4F, 0x15, 0x8B,
	0x88, 0x00, 0x60, 0x07, 0x19, 0xCB, 0x29, 0xDA, 0xB8, 0x34, 0xC2, 0xBF, 0xE5, 0x2E, 0xD1, 0x76,
	0x00, 0x18, 0xEC, 0xF3, 0x6B, 0x01, 0x60, 0xF0, 0xFF, 0xA8, 0x39, 0x54, 0x2E, 0x4B, 0x00, 0x49,
	0x2D, 0x81, 0x28, 0x78, 0x4C, 0xC0, 0x00, 0x80, 0x00, 0x04, 0xFA, 0x98, 0x00, 0x18, 0xAC, 0xDF,
	0x6A, 0x81, 0x22, 0xB0, 0xD7, 0xF1, 0x63, 0xB0, 0x28, 0x00, 0xAF, 0x18, 0xC4, 0xB8, 0x02, 0xAC,
	0xE5, 0xC4, 0x21, 0x79, 0x25, 0x40, 0x04, 0x62, 0x2E, 0x32, 0x95, 0xF3, 0x74, 0x1E, 0xE0, 0x39,
	0x70, 0x00, 0x18, 0xAC, 0xE3, 0x69, 0xE5, 0x8E, 0x9C, 0x70, 0x78, 0x26, 0xA2, 0x66, 0x55, 0x59,
	0x1D, 0xE3, 0x9E, 0xDA, 0x4A, 0xB4, 0xA3, 0x7C, 0x57, 0x25, 0xEB, 0xD3, 0x8D, 0xDA, 0x00, 0x18,
	0x6C, 0xDF, 0x6A, 0xBD, 0x61, 0xD0, 0xCB, 0x30, 0xA4, 0x30, 0xA8, 0x00, 0x08, 0x6C, 0xBB, 0x6A,
	0xBF, 0x0D, 0x1D, 0x00, 0x44, 0x25, 0x73, 0xDB, 0x55, 0x5F, 0x3C, 0xF3, 0x2E, 0xA7, 0x68, 0x00,
	0x6F, 0x18, 0x84, 0x28, 0x02, 0x42, 0x04, 0x47, 0xE7, 0xDD, 0x25, 0x80, 0x19, 0x20, 0x25, 0x3F,
	0x05, 0xC0, 0x00, 0x18, 0x3C, 0xBB, 0x6B, 0x43, 0x60, 0xF1, 0xB7, 0xE9, 0x58, 0x26, 0x9E, 0x65,
	0xBB, 0x4C, 0x17, 0x17, 0x91, 0x7F, 0xAE, 0xCE, 0xA1, 0x40, 0x00, 0x18, 0xCC, 0xDF, 0x68, 0x40,
	0xD0, 0xF0, 0xE0, 0xCC, 0x7A, 0xD4, 0x2E, 0x66, 0xFB, 0x46, 0x87, 0x82, 0x0B, 0x04, 0x6B, 0xFA,
	0x89, 0x53, 0x37, 0xDA, 0x90, 0xB4, 0x3C, 0x98, 0x13, 0x5E, 0x8C, 0xB3, 0x93, 0xA0, 0x80, 0x00,
	0x18, 0x3C, 0x10, 0xE5, 0x2E, 0x08, 0x00, 0x82, 0xBC, 0x00, 0x8E, 0x59, 0x4E, 0xD1, 0x81, 0xA1,
	0xE1, 0xBE, 0xC2, 0xF8, 0xE0, 0x90, 0x00, 0x08, 0xCC, 0x77, 0x6B, 0x95, 0xDD, 0x01, 0xA0, 0x94,
	0xA4, 0x53, 0xA5, 0x78, 0x33, 0xAD, 0xDA, 0x8A, 0xE7, 0x3F, 0xFD, 0x57, 0x39, 0xF8, 0x23, 0xB9,
	0xDA, 0x6A, 0xB9, 0x3F, 0x02, 0x57, 0x49, 0xFF, 0xC0, 0x00, 0x18, 0xCC, 0xB7, 0x6B, 0xA7, 0x9D,
	0x81, 0x08, 0xD5, 0xC4, 0x92, 0xE2, 0x51, 0xB9, 0x38, 0x00, 0x00, 0x18, 0xCC, 0xAF, 0x69, 0xE5,
	0xBD, 0xEF, 0x78, 0xE5, 0x21, 0xA4, 0xE2, 0x55, 0x6B, 0x79, 0x52, 0x8B, 0x7C, 0xCA, 0x00, 0x18,
	0x1C, 0x2F, 0x6A, 0x91, 0x63, 0x08, 0xDF, 0x0C, 0x1A, 0xD2, 0x68, 0x00, 0x18, 0xCC, 0xAF, 0x69,
	0xCD, 0xF5, 0xFE, 0xA8, 0xD9, 0xC4, 0x11, 0x12, 0x75, 0x7B, 0x49, 0x55, 0xF6, 0x7F, 0x00, 0x18,
	0xCC, 0xE7, 0x6A, 0xA0, 0xD3, 0x31, 0x90, 0x4C, 0x78, 0x73, 0x8A, 0x4F, 0x80, 0x08, 0x04, 0xFF,
	0x70, 0xC7, 0x94, 0xF0, 0x1F, 0xE3, 0xFD, 0x1E, 0x0B, 0x24, 0x4F, 0x00, 0x49, 0xD4, 0xEF, 0xFF,
	0x3A, 0x66, 0x77, 0xB4, 0x48, 0x69, 0x31, 0x20, 0x06, 0x3E, 0x58, 0x66, 0x24, 0x81, 0xF8, 0x02,
	0x00, 0xF0, 0xF9, 0x00, 0x00, 0x18, 0x9C, 0x77, 0x68, 0xFE, 0x92, 0xB0, 0x8F, 0x65, 0xDB, 0x33,
	0xA8, 0x00, 0x18, 0x64, 0xE0, 0x02, 0xA4, 0xE4, 0x81, 0x86, 0xBB, 0x21, 0x40, 0x1D, 0x67, 0xCC,
	0x16, 0x23, 0xD1, 0x28, 0x00, 0x61, 0x27, 0xAF, 0xF9, 0x80, 0x00, 0x18, 0xCC, 0x83, 0x6B, 0x7C,
	0xE3, 0x08, 0xA7, 0x40, 0xC3, 0x70, 0x8C, 0x63, 0xBB, 0x54, 0x93, 0x05, 0x9E, 0xFA, 0xE1, 0xCD,
	0x86, 0x40, 0x00, 0x98, 0x24, 0xB0, 0x01, 0x8C, 0x48, 0x46, 0xE5, 0x38, 0x00, 0x18, 0x1C, 0xB7,
	0x6B, 0xFE, 0x92, 0xD2, 0xC3, 0xC0, 0xC3, 0xD1, 0x28, 0x00, 0x18, 0x1C, 0xB7, 0x69, 0xBD, 0xE0,
	0x90, 0xDB, 0x60, 0x24, 0x35, 0xC9, 0x63, 0xBB, 0x57, 0x14, 0x9B, 0x86, 0x7E, 0x62, 0xF2, 0x99,
	0x7A, 0x5C, 0x10, 0x09, 0x20, 0x07, 0x77, 0x74, 0x00, 0x08, 0x1C, 0xE7, 0x69, 0xF5, 0xAE, 0xFC,
	0xB0, 0x44, 0x25, 0x51, 0x2D, 0x9F, 0xCA, 0xAE, 0x68, 0xFE, 0x54, 0x75, 0x46, 0x0E, 0xAB, 0x0B,
	0xD0, 0x00, 0x00, 0x2F, 0x18, 0xC4, 0xF0, 0x00, 0x94, 0x99, 0x46, 0x27, 0x3E, 0x63, 0x7B, 0x57,
	0x14, 0x98, 0x46, 0xFC, 0xE9, 0xEE, 0xAE, 0x40, 0x00, 0x1F, 0x18, 0xA4, 0xF0, 0x02, 0x4C, 0x85,
	0x7A, 0xA2, 0xBE, 0x65, 0x3B, 0x58, 0x95, 0x93, 0x16, 0xF9, 0xA5, 0xC5, 0x8E, 0x40, 0x00, 0x18,
	0x6C, 0x33, 0x6B, 0x42, 0xD0, 0xAA, 0x4F, 0x2C, 0x9F, 0x10, 0xAB, 0x67, 0xDB, 0x45, 0x06, 0x95,
	0x42, 0x7D, 0x82, 0xD7, 0x1E, 0xD3, 0x29, 0xDA, 0x6F, 0x38, 0x24, 0xB2, 0xCC, 0x69, 0x49, 0xC6,
	0x00, 0x2F, 0x18, 0xC4, 0x30, 0x01, 0xA8, 0x1F, 0xFC, 0x16, 0xFE, 0x67, 0xBB, 0x45, 0x0C, 0x9B,
	0x8C, 0xFF, 0xAB, 0xCE, 0xA1, 0x40, 0x00, 0x18, 0x64, 0x20, 0x01, 0x78, 0xE1, 0x81, 0xD6, 0xFE,
	0x23, 0x40, 0x19, 0x24, 0x26, 0x1A, 0x97, 0xE6, 0x13, 0x68, 0x03, 0xC8, 0x28, 0x82, 0x4A, 0x80,
	0x32, 0x4E, 0x84, 0x19, 0x0F, 0x80, 0x00, 0x08, 0xEC, 0xA7, 0x6B, 0x0C, 0x9D, 0xFF, 0x68, 0x25,
	0x20, 0xC6, 0xA1, 0xC0, 0x00, 0x04, 0x21, 0x3F, 0x1F, 0xC0, 0x00, 0x18, 0x4C, 0xF3, 0x68, 0xC3,
	0x13, 0x72, 0x7B, 0x35, 0xE9, 0x51, 0x2E, 0x63, 0x5B, 0x4D, 0x7D, 0x18, 0x87, 0xE9, 0x0E, 0x2E,
	0xA1, 0x4A, 0x78, 0x1C, 0x70, 0xB8, 0x0C, 0x8F, 0xA0, 0x00, 0x98, 0x84, 0xF0, 0x03, 0x94, 0x29,
	0x07, 0x26, 0x38, 0x00, 0xB8, 0x4F, 0x18, 0x84, 0xF0, 0x01, 0xAC, 0x91, 0x03, 0x53, 0xDA, 0x6D,
	0x03, 0xBB, 0x1E, 0x5E, 0x00, 0xEF, 0x18, 0x84, 0xB8, 0x03, 0x8C, 0x8D, 0x87, 0x62, 0x3C, 0x27,
	0x00, 0x05, 0xA6, 0xC2, 0x08, 0xA7, 0xE5, 0x2A, 0x00, 0xAD, 0x11, 0x61, 0xB1, 0xCE, 0x00, 0x18,
	0x6C, 0x2F, 0x6A, 0x91, 0x8A, 0xEB, 0x40, 0x55, 0xDD, 0x97, 0x2C, 0xA3, 0x40, 0x1B, 0x10, 0x0D,
	0xC3, 0x21, 0x0B, 0x2B, 0xDA, 0xA4, 0xA2, 0x32, 0x10, 0x03, 0x77, 0xD9, 0x3A, 0x00, 0x18, 0xAC,
	0xF7, 0x68, 0xEC, 0xF4, 0xFF, 0x60, 0x79, 0xA2, 0xD5, 0x56, 0x51, 0xAF, 0x6E, 0xDA, 0xB9, 0x6B,
	0xF6, 0x35, 0x4B, 0xDA, 0xB0, 0x00, 0x18, 0xCC, 0x7B, 0x69, 0xE5, 0xCC, 0xC1, 0x98, 0x44, 0xA5,
	0x23, 0xA6, 0x55, 0xA5, 0x9C, 0x32, 0x8D, 0x28, 0xC0, 0x00, 0x18, 0xAC, 0xB3, 0x69, 0x7C, 0xE3,
	0x53, 0x83, 0x3E, 0x42, 0xD0, 0x2D, 0x67, 0x9B, 0x5A, 0x0C, 0x87, 0x91, 0xF8, 0xA3, 0x7E, 0x9E,
	0x5B, 0x2A, 0xDA, 0xA0, 0x78, 0xA4, 0x75, 0x60, 0x50, 0x9D, 0x92, 0x00, 0x18, 0xCC, 0xB7, 0x69,
	0x95, 0xDD, 0xFD, 0x00, 0x95, 0xC4, 0xD2, 0x12, 0x75, 0xBB, 0x49, 0x5C, 0x96, 0xEF, 0x4A, 0xB4,
	0xA7, 0x7A, 0x53, 0xB5, 0x3B, 0xD2, 0xAD, 0xAE, 0xEF, 0x9C, 0x6E, 0xD4, 0x77, 0x3B, 0x59, 0x5F,
	0x55, 0x67, 0x8A, 0x38, 0xAB, 0x3A, 0x53, 0xD5, 0x5D, 0xE3, 0xBA, 0xDA, 0xCA, 0xAA, 0xBB, 0xB8,
	0x00, 0x18, 0x64, 0xC0, 0x00, 0x84, 0x71, 0xF8, 0xA2, 0x5E, 0xA6, 0x00, 0x1D, 0x21, 0x88, 0x2A,
	0x8B, 0x53, 0x1E, 0xDA, 0x3F, 0xEC, 0x44, 0xE8, 0xDF, 0x91, 0x75, 0x8A, 0x99, 0x4E, 0xD3, 0xFD,
	0xA5, 0xA5, 0x9B, 0x83, 0x81, 0xAC, 0x50, 0x00, 0x18, 0xCC, 0xE7, 0x6A, 0xA0, 0xD3, 0x31, 0x90,
	0x4C, 0x78, 0x73, 0x8A, 0x4F, 0x80, 0x08, 0x04, 0xFF, 0x70, 0xC7, 0x94, 0xF0, 0x1F, 0xE3, 0xFD,
	0x1E, 0x0B, 0x24, 0x4F, 0x00, 0x49, 0xD4, 0xEF, 0xFF, 0x3A, 0x00, 0x00, 0xAF, 0x18, 0xA4, 0xA0,
	0x00, 0x54, 0xE4, 0x84, 0xC1, 0x3D, 0xA5, 0x40, 0x0B, 0x66, 0x62, 0x24, 0x9B, 0xC0, 0x00, 0x18,
	0x8C, 0xB7, 0x68, 0xE0, 0x91, 0x30, 0xE7, 0x2D, 0x9F, 0xD5, 0x2A, 0x48, 0x00, 0x00, 0x3F, 0xE0,
	0x70, 0x00, 0x46, 0x5B, 0xB4, 0xCE, 0x70, 0x48, 0x65, 0xC8, 0x93, 0x68, 0x56, 0x27, 0xC0, 0x04,
	0x02, 0x00, 0x3F, 0x16, 0xE2, 0x74, 0x00, 0x40, 0x04, 0x00, 0x07, 0xA1, 0x26, 0x80, 0x07, 0x00,
	0x70, 0x28, 0x15, 0xE2, 0x64, 0x00, 0x40, 0x04, 0x83, 0x7A, 0x3D, 0x33, 0xED, 0xA7, 0x84, 0x48,
	0x43, 0x1C, 0xB5, 0x6E, 0xC0, 0xB1, 0x34, 0x00, 0x07, 0x88, 0x44, 0x05, 0x70, 0x00, 0x18, 0x4C,
	0xB7, 0x68, 0xE0, 0x91, 0x30, 0xE7, 0x2D, 0x9F, 0xD5, 0x2A, 0x48, 0x00, 0x00, 0x3F, 0xE0, 0x70,
	0x00, 0x26, 0x1B, 0xB4, 0xCE, 0x70, 0x68, 0x65, 0x88, 0x92, 0x6A, 0x55, 0x24, 0x00, 0x00, 0x03,
	0x80, 0x80, 0x47, 0xD2, 0x74, 0x00, 0x40, 0x20, 0x03, 0xF1, 0x6D, 0x24, 0x00, 0x04, 0x00, 0x40,
	0x00, 0x7A, 0x12, 0x40, 0x00, 0x70, 0x07, 0x02, 0x81, 0x5F, 0x25, 0xC0, 0x04, 0x00, 0x48, 0x37,
	0xA3, 0xF3, 0x1E, 0xDA, 0x78, 0x44, 0x84, 0x31, 0xCB, 0x56, 0xEC, 0x0B, 0x93, 0x00, 0x00, 0x78,
	0x84, 0x40, 0x57, 0x00, 0x00, 0x08, 0xCC, 0xB7, 0x69, 0xD5, 0x8D, 0xFC, 0xC2, 0x0D, 0x26, 0x51,
	0xB5, 0x18, 0x23, 0xB9, 0xDA, 0xAA, 0x78, 0xBD, 0x04, 0x73, 0xDB, 0x4D, 0xDD, 0x6D, 0x00, 0x00,
	0x98, 0x8C, 0xBB, 0x6B, 0x43, 0x60, 0xF1, 0xB7, 0xE9, 0x58, 0x26, 0x9E, 0x65, 0xBB, 0x4C, 0x17,
	0x17, 0x91, 0x7F, 0xAE, 0xCE, 0xA1, 0x40, 0x00, 0x18, 0x54, 0xE8, 0x00, 0x52, 0x38, 0x24, 0xA6,
	0x38, 0x00, 0x18, 0x2C, 0xBB, 0x6B, 0x01, 0x60, 0xF0, 0xFF, 0xA8, 0x39, 0x54, 0x2A, 0x4C, 0x00,
	0x49, 0x2D, 0x81, 0x28, 0x78, 0x24, 0xD0, 0x00, 0x80, 0x00, 0x04, 0xFA, 0x9E, 0x66, 0xFB, 0x45,
	0x07, 0x19, 0x96, 0xFC, 0x6E, 0xC2, 0x85, 0x62, 0x40, 0x0C, 0x01, 0x00, 0x06, 0x7A, 0x13, 0x24,
	0x01, 0x95, 0x00, 0x40, 0x7F, 0xC0, 0x00, 0x00, 0x18, 0x74, 0xC0, 0x01, 0x84, 0xF0, 0x00, 0x94,
	0x19, 0x63, 0xDB, 0x44, 0x0C, 0x97, 0x98, 0xBD, 0x01, 0xE4, 0x90, 0xC0, 0x00, 0x18, 0x4C, 0x6B,
	0x69, 0x95, 0xD5, 0xFC, 0x88, 0x0C, 0x27, 0x57, 0x64, 0x51, 0x45, 0xAB, 0xF3, 0xAA, 0xDA, 0xCA,
	0xF8, 0xAD, 0x7D, 0x55, 0x25, 0x1D, 0xCA, 0xB9, 0xAC, 0xCE, 0x55, 0xCD, 0x56, 0x78, 0xAF, 0x8A,
	0xB3, 0xE7, 0x35, 0xB4, 0x00, 0x18, 0x4C, 0x27, 0x69, 0xE0, 0xD0, 0x22, 0x4F, 0x4D, 0xEC, 0xD1,
	0xAA, 0x4C, 0x00, 0x71, 0xE4, 0x01, 0xE1, 0x7C, 0x24, 0xC0, 0x38, 0xE0, 0x41, 0xEF, 0x00, 0x82,
	0x65, 0x3B, 0x55, 0xED, 0x14, 0x86, 0x1C, 0x02, 0x22, 0x9B, 0x52, 0x40, 0x13, 0x88, 0x20, 0x8F,
	0xB9, 0xA3, 0x32, 0x9D, 0xA7, 0x80, 0x8C, 0x4D, 0x5E, 0x32, 0x65, 0x55, 0xB9, 0x9F, 0x6D, 0x00,
	0x3C, 0x56, 0x1C, 0xE6, 0xB3, 0x6A, 0xAD, 0xC9, 0xF0, 0x00, 0x07, 0xE0, 0x12, 0x31, 0x7C, 0xC3,
	0xB6, 0x9D, 0xDA, 0x11, 0x0F, 0x37, 0xF0, 0x55, 0x26, 0x80, 0x00, 0x18, 0xCC, 0xF7, 0x6B, 0xA6,
	0xBF, 0x03, 0x20, 0xE4, 0xA4, 0x14, 0xE2, 0x51, 0x69, 0xF8, 0x33, 0xAB, 0xDA, 0x4A, 0xB5, 0x2F,
	0x06, 0x55, 0xB9, 0xBF, 0xCA, 0x99, 0x29, 0xDF, 0x54, 0xC9, 0x76, 0xFA, 0xEA, 0x76, 0x80, 0x00,
	0x2F, 0x18, 0x24, 0xE0, 0x03, 0x84, 0x04, 0x39, 0xC3, 0x3A, 0x63, 0x3B, 0x47, 0xEF, 0x17, 0x98,
	0xFB, 0xA6, 0xC3, 0xA1, 0x40, 0x00, 0x18, 0xAC, 0x77, 0x68, 0xE2, 0xD1, 0x12, 0xFF, 0x0D, 0x19,
	0x36, 0x2E, 0x63, 0x3B, 0x51, 0x0E, 0x87, 0x87, 0xFC, 0x60, 0xC1, 0xB1, 0x7B, 0x09, 0xDA, 0x74,
	0x98, 0xCC, 0x30, 0x03, 0x06, 0x2C, 0x0A, 0x00, 0x2F, 0x18, 0x84, 0x60, 0x01, 0x84, 0x49, 0xFE,
	0x13, 0xDA, 0x65, 0x7B, 0x54, 0x85, 0x93, 0x16, 0xFF, 0x61, 0xCD, 0xA6, 0x40, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};



//**************************************************************************
//  LIVE DEVICE
//**************************************************************************

static VOID lpc12_update(struct lpc12_t *f);
static void lpc12_regdec(struct lpc12_t *f);
static ULONG bitrev32(ULONG val);
static UBYTE bitrev8(UBYTE val);
static void bitrevbuff(const UBYTE *src, UBYTE *dst, unsigned int start, unsigned int length);
static ULONG getb(struct sp0256_t *sp, int len);
static VOID micro(struct sp0256_t *sp);


//-------------------------------------------------
//  sp0256_create - device-specific creation
//-------------------------------------------------

struct sp0256_t * sp0256_create(int clock_hz, const UBYTE *rom, USHORT size)
{
    struct sp0256_t *sp = (struct sp0256_t*)AllocVec(sizeof(struct sp0256_t), MEMF_PUBLIC|MEMF_CLEAR);

    if(sp)
    {
        /* -------------------------------------------------------------------- */
        /*  Configure our internal variables.                                   */
        /* -------------------------------------------------------------------- */
        sp->filt.rng = 1;
        sp0256_update_clock(sp, clock_hz);

        /* -------------------------------------------------------------------- */
        /*  Set up the microsequencer's initial state.                          */
        /* -------------------------------------------------------------------- */
        sp->halted   = 1;
        sp->filt.rpt = -1;
        sp->lrq      = 0x8000;
        sp->page     = 0x1000 << 3;
        sp->silent   = 1;

        /* -------------------------------------------------------------------- */
        /*  Setup the ROM.                                                      */
        /* -------------------------------------------------------------------- */
        // the rom is not supposed to be reversed first; according to Joe Zbiciak.
        // see http://forums.bannister.org/ubbthreads.php?ubb=showflat&Number=72385#Post72385
        if(rom == SP0256_ROM_AL2)
        {
            rom = sp0256_al2_rom;
            size = sizeof(sp0256_al2_rom);
        }

        sp->rom = (UBYTE*)AllocVec(size, MEMF_PUBLIC);

        if(sp->rom)
        {
            sp->size = size;
            bitrevbuff(rom, sp->rom, 0, size);
        }
        else
        {
            sp0256_destroy(sp);
            sp = NULL;
        }
    }

    return sp;
}


//-------------------------------------------------
//  sp0256_destroy - device-specific destruction
//-------------------------------------------------

VOID sp0256_destroy(struct sp0256_t *sp)
{
    FreeVec(sp->rom);
    FreeVec(sp);
}


//-------------------------------------------------
//  sp0256_update_clock - set a new clock at runtime
//-------------------------------------------------

VOID sp0256_update_clock(struct sp0256_t *sp, int clock_hz)
{
    sp->tick_ref = (156 * 10000000 / clock_hz * 200);
}



//-------------------------------------------------
//  sp0256_reset - device-specific reset
//-------------------------------------------------

VOID sp0256_reset(struct sp0256_t *sp)
{
    // reset FIFO and SP0256
    sp->fifo_head = sp->fifo_tail = sp->fifo_bitp = 0;

    memset(&sp->filt, 0, sizeof(sp->filt));
    sp->halted   = 1;
    sp->filt.rpt = -1;
    sp->filt.rng = 1;
    sp->lrq      = 0;
    sp->ald      = 0x0000;
    sp->pc       = 0x0000;
    sp->stack    = 0x0000;
    sp->fifo_sel = 0;
    sp->mode     = 0;
    sp->page     = 0x1000 << 3;
    sp->silent   = 1;
    sp->sby_line = 1;
}


//-------------------------------------------------
//  sp0256_tick - device tick (1 microsecond)
//-------------------------------------------------

VOID sp0256_tick(struct sp0256_t *sp)
{
    sp->tick += 1000;

    while(sp->tick > sp->tick_ref)
    {
        sp->tick -= sp->tick_ref;

        if(!sp->lrq) LOG(("sp0256: lrq = 0\n"));

        /* ------------------------------------------------------------ */
        /*  Emulate the microsequencer.                                 */
        /* ------------------------------------------------------------ */
        micro(sp);

        /* ------------------------------------------------------------ */
        /*  Update the 12-pole filter.                                  */
        /* ------------------------------------------------------------ */
        lpc12_update(&sp->filt);
    }
}


//-------------------------------------------------
//  sp0256_get_audio - get current audio output
//-------------------------------------------------

SHORT sp0256_get_audio(struct sp0256_t *sp)
{
    if(sp->silent)
        return 0;
    else
        return sp->filt.out;
}


VOID sp0256_write_ald(struct sp0256_t *sp, UBYTE data)
{
    /* ---------------------------------------------------------------- */
    /*  Drop writes to the ALD register if we're busy.                  */
    /* ---------------------------------------------------------------- */
    if (!sp->lrq)
    {
        LOG(("sp0256: Droped ALD write\n"));
        return;
    }

    /* ---------------------------------------------------------------- */
    /*  Set LRQ to "busy" and load the 8 LSBs of the data into the ALD  */
    /*  reg.  We take the command address, and multiply by 2 bytes to   */
    /*  get the new PC address.                                         */
    /* ---------------------------------------------------------------- */
    sp->lrq = 0;
    sp->ald = (data << 4) | (0x1000 << 3);
    sp->sby_line = 0;

    /* ---------------------------------------------------------------- */
    /*  LRQ should remain busy for at least 15940 ns as stated here:    */
    /*  http://www.cpcwiki.eu/index.php/SP0256_Measured_Timings         */
    /* ---------------------------------------------------------------- */
    if(sp->halted)
    {
        sp->tick = sp->tick_ref - 15940;
    }
}

UBYTE sp0256_read_lrq(struct sp0256_t *sp)
{
    LOG(("sp0256: read lrq(%d)\n", (sp->lrq == 0x8000 ? 1 : 0)));

    return sp->lrq == 0x8000;
}

UBYTE sp0256_read_sby(struct sp0256_t *sp)
{
    return sp->sby_line;
}

USHORT sp0256_read_spb640_lrq(struct sp0256_t *sp)
{
    /* -------------------------------------------------------------------- */
    /*  Return the SP0256 LRQ status on bit 15.                             */
    /* -------------------------------------------------------------------- */
    return sp->lrq;
}

USHORT sp0256_read_spb640_fifo(struct sp0256_t *sp)
{
    /* -------------------------------------------------------------------- */
    /*  Return the SPB640 FIFO full status on bit 15.                       */
    /* -------------------------------------------------------------------- */
    return (sp->fifo_head - sp->fifo_tail) >= 64 ? 0x8000 : 0;
}

VOID sp0256_write_spb640_ald(struct sp0256_t *sp, USHORT data)
{
    sp0256_write_ald(sp, data & 0xff);
}

VOID sp0256_write_spb640_fifo(struct sp0256_t *sp, USHORT data)
{
    /* ---------------------------------------------------------------- */
    /*  If Bit 10 is set, reset the FIFO, and SP0256.                   */
    /* ---------------------------------------------------------------- */
    if (data & 0x400)
    {
        sp->fifo_head = sp->fifo_tail = sp->fifo_bitp = 0;
        sp0256_reset(sp);
        return;
    }

    /* ---------------------------------------------------------------- */
    /*  If the FIFO is full, drop the data.                             */
    /* ---------------------------------------------------------------- */
    if ((sp->fifo_head - sp->fifo_tail) >= 64)
    {
        LOG(("spb640: Dropped FIFO write\n"));
        return;
    }

    /* ---------------------------------------------------------------- */
    /*  FIFO up the lower 10 bits of the data.                          */
    /* ---------------------------------------------------------------- */
    LOG(("spb640: WR_FIFO %.3X %d.%d %d\n", data & 0x3ff,
            sp->fifo_tail, sp->fifo_bitp, sp->fifo_head));

    sp->fifo[sp->fifo_head++ & 63] = data & 0x3ff;
}



/* ======================================================================== */
/*  LPC12_UPDATE     -- Update the 12-pole filter, outputting current audio */
/* ======================================================================== */
static VOID lpc12_update(struct lpc12_t *f)
{
    if(f->rpt)
    {
        int i;
        SHORT samp = 0;
        int do_int = 0;

        /* ---------------------------------------------------------------- */
        /*  Generate a series of periodic impulses, or random noise.        */
        /* ---------------------------------------------------------------- */
        if (f->per)
        {
            if (f->cnt <= 0)
            {
                f->cnt += f->per;
                samp    = f->amp;
                f->rpt--;
                do_int  = f->interp;

                for (i = 0; i < 6; i++)
                    f->z_data[i][1] = f->z_data[i][0] = 0;

            } else
            {
                samp = 0;
                f->cnt--;
            }

        } else
        {
            int bit;

            if (--f->cnt <= 0)
            {
                do_int = f->interp;
                f->cnt = PER_NOISE;
                f->rpt--;
                for (i = 0; i < 6; i++)
                    f->z_data[i][0] = f->z_data[i][1] = 0;
            }

            bit = f->rng & 1;
            f->rng = (f->rng >> 1) ^ (bit ? 0x4001 : 0);

            if (bit) { samp =  f->amp; }
            else     { samp = -f->amp; }
        }

        /* ---------------------------------------------------------------- */
        /*  If we need to, process the interpolation registers.             */
        /* ---------------------------------------------------------------- */
        if (do_int)
        {
            f->r[0] += f->r[14];
            f->r[1] += f->r[15];

            f->amp   = (f->r[0] & 0x1F) << (((f->r[0] & 0xE0) >> 5) + 0);
            f->per   = f->r[1];

            do_int   = 0;
        }

        /* ---------------------------------------------------------------- */
        /*  Stop if we expire our repeat counter                            */
        /* ---------------------------------------------------------------- */
        if (f->rpt <= 0) return;

        /* ---------------------------------------------------------------- */
        /*  Each 2nd order stage looks like one of these.  The App. Manual  */
        /*  gives the first form, the patent gives the second form.         */
        /*  They're equivalent except for time delay.  I implement the      */
        /*  first form.   (Note: 1/Z == 1 unit of time delay.)              */
        /*                                                                  */
        /*          ---->(+)-------->(+)----------+------->                 */
        /*                ^           ^           |                         */
        /*                |           |           |                         */
        /*                |           |           |                         */
        /*               [B]        [2*F]         |                         */
        /*                ^           ^           |                         */
        /*                |           |           |                         */
        /*                |           |           |                         */
        /*                +---[1/Z]<--+---[1/Z]<--+                         */
        /*                                                                  */
        /*                                                                  */
        /*                +---[2*F]<---+                                    */
        /*                |            |                                    */
        /*                |            |                                    */
        /*                v            |                                    */
        /*          ---->(+)-->[1/Z]-->+-->[1/Z]---+------>                 */
        /*                ^                        |                        */
        /*                |                        |                        */
        /*                |                        |                        */
        /*                +-----------[B]<---------+                        */
        /*                                                                  */
        /* ---------------------------------------------------------------- */
        for (i = 0; i < 6; i++)
        {
            samp += (((int)f->b_coef[i] * (int)f->z_data[i][1]) >> 9);
            samp += (((int)f->f_coef[i] * (int)f->z_data[i][0]) >> 8);

            f->z_data[i][1] = f->z_data[i][0];
            f->z_data[i][0] = samp;
        }

        /* ------------------------------------------------------------ */
        /*  Compute new output audio level from filter output.          */
        /* ------------------------------------------------------------ */
        if (samp > 4095)
            f->out = 32767;
        else if (samp < -4096)
            f->out = -32768;
        else
            f->out = samp * 8;
    }
}

static const int stage_map[6] = { 0, 1, 2, 3, 4, 5 };

/* ======================================================================== */
/*  LPC12_REGDEC -- Decode the register set in the filter bank.             */
/* ======================================================================== */
static void lpc12_regdec(struct lpc12_t *f)
{
    int i;

    /* -------------------------------------------------------------------- */
    /*  Decode the Amplitude and Period registers.  Force the 'cnt' to 0    */
    /*  to get an initial impulse.  We compensate elsewhere by setting      */
    /*  the repeat count to "repeat + 1".                                   */
    /* -------------------------------------------------------------------- */
    f->amp = (f->r[0] & 0x1F) << (((f->r[0] & 0xE0) >> 5) + 0);
    f->cnt = 0;
    f->per = f->r[1];

    /* -------------------------------------------------------------------- */
    /*  Decode the filter coefficients from the quant table.                */
    /* -------------------------------------------------------------------- */
    for (i = 0; i < 6; i++)
    {
        #define IQ(x) (((x) & 0x80) ? qtbl[0x7F & -(x)] : -qtbl[(x)])

        f->b_coef[stage_map[i]] = IQ(f->r[2 + 2*i]);
        f->f_coef[stage_map[i]] = IQ(f->r[3 + 2*i]);
    }

    /* -------------------------------------------------------------------- */
    /*  Set the Interp flag based on whether we have interpolation parms    */
    /* -------------------------------------------------------------------- */
    f->interp = f->r[14] || f->r[15];

    return;
}

/* ======================================================================== */
/*  SP0256_DATAFMT   -- Data format table for the SP0256's microsequencer   */
/*                                                                          */
/*  len     4 bits      Length of field to extract                          */
/*  lshift  4 bits      Left-shift amount on field                          */
/*  param   4 bits      Parameter number being updated                      */
/*  delta   1 bit       This is a delta-update.  (Implies sign-extend)      */
/*  field   1 bit       This is a field replace.                            */
/*  clr5    1 bit       Clear F5, B5.                                       */
/*  clrall  1 bit       Clear all before doing this update                  */
/* ======================================================================== */

#define CR(l,s,p,d,f,c5,ca)         \
        (                           \
            (((l)  & 15) <<  0) |   \
            (((s)  & 15) <<  4) |   \
            (((p)  & 15) <<  8) |   \
            (((d)  &  1) << 12) |   \
            (((f)  &  1) << 13) |   \
            (((c5) &  1) << 14) |   \
            (((ca) &  1) << 15)     \
        )

#define CR_DELTA  CR(0,0,0,1,0,0,0)
#define CR_FIELD  CR(0,0,0,0,1,0,0)
#define CR_CLR5   CR(0,0,0,0,0,1,0)
#define CR_CLRA   CR(0,0,0,0,0,0,1)
#define CR_LEN(x) ((x) & 15)
#define CR_SHF(x) (((x) >> 4) & 15)
#define CR_PRM(x) (((x) >> 8) & 15)

enum { AM = 0, PR, B0, F0, B1, F1, B2, F2, B3, F3, B4, F4, B5, F5, IA, IP };

static const USHORT sp0256_datafmt[] =
{
    /* -------------------------------------------------------------------- */
    /*  OPCODE 1111: PAUSE                                                  */
    /* -------------------------------------------------------------------- */
    /*    0 */  CR( 0,  0,  0,  0,  0,  0,  1),     /*  Clear all   */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0001: LOADALL                                                */
    /* -------------------------------------------------------------------- */
                /* All modes                */
    /*    1 */  CR( 8,  0,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*    2 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*    3 */  CR( 8,  0,  B0, 0,  0,  0,  0),     /*  B0          */
    /*    4 */  CR( 8,  0,  F0, 0,  0,  0,  0),     /*  F0          */
    /*    5 */  CR( 8,  0,  B1, 0,  0,  0,  0),     /*  B1          */
    /*    6 */  CR( 8,  0,  F1, 0,  0,  0,  0),     /*  F1          */
    /*    7 */  CR( 8,  0,  B2, 0,  0,  0,  0),     /*  B2          */
    /*    8 */  CR( 8,  0,  F2, 0,  0,  0,  0),     /*  F2          */
    /*    9 */  CR( 8,  0,  B3, 0,  0,  0,  0),     /*  B3          */
    /*   10 */  CR( 8,  0,  F3, 0,  0,  0,  0),     /*  F3          */
    /*   11 */  CR( 8,  0,  B4, 0,  0,  0,  0),     /*  B4          */
    /*   12 */  CR( 8,  0,  F4, 0,  0,  0,  0),     /*  F4          */
    /*   13 */  CR( 8,  0,  B5, 0,  0,  0,  0),     /*  B5          */
    /*   14 */  CR( 8,  0,  F5, 0,  0,  0,  0),     /*  F5          */
                /* Mode 01 and 11 only      */
    /*   15 */  CR( 8,  0,  IA, 0,  0,  0,  0),     /*  Amp Interp  */
    /*   16 */  CR( 8,  0,  IP, 0,  0,  0,  0),     /*  Pit Interp  */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0100: LOAD_4                                                 */
    /* -------------------------------------------------------------------- */
                /* Mode 00 and 01           */
    /*   17 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*   18 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*   19 */  CR( 4,  3,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*   20 */  CR( 6,  2,  F3, 0,  0,  0,  0),     /*  F3          */
    /*   21 */  CR( 7,  1,  B4, 0,  0,  0,  0),     /*  B4          */
    /*   22 */  CR( 6,  2,  F4, 0,  0,  0,  0),     /*  F4          */
                /* Mode 01 only             */
    /*   23 */  CR( 8,  0,  B5, 0,  0,  0,  0),     /*  B5          */
    /*   24 */  CR( 8,  0,  F5, 0,  0,  0,  0),     /*  F5          */

                /* Mode 10 and 11           */
    /*   25 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*   26 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*   27 */  CR( 6,  1,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*   28 */  CR( 7,  1,  F3, 0,  0,  0,  0),     /*  F3          */
    /*   29 */  CR( 8,  0,  B4, 0,  0,  0,  0),     /*  B4          */
    /*   30 */  CR( 8,  0,  F4, 0,  0,  0,  0),     /*  F4          */
                /* Mode 11 only             */
    /*   31 */  CR( 8,  0,  B5, 0,  0,  0,  0),     /*  B5          */
    /*   32 */  CR( 8,  0,  F5, 0,  0,  0,  0),     /*  F5          */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0110: SETMSB_6                                               */
    /* -------------------------------------------------------------------- */
                /* Mode 00 only             */
    /*   33 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 00 and 01           */
    /*   34 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*   35 */  CR( 6,  2,  F3, 0,  1,  0,  0),     /*  F3 (5 MSBs) */
    /*   36 */  CR( 6,  2,  F4, 0,  1,  0,  0),     /*  F4 (5 MSBs) */
                /* Mode 01 only             */
    /*   37 */  CR( 8,  0,  F5, 0,  1,  0,  0),     /*  F5 (5 MSBs) */

                /* Mode 10 only             */
    /*   38 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 10 and 11           */
    /*   39 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*   40 */  CR( 7,  1,  F3, 0,  1,  0,  0),     /*  F3 (6 MSBs) */
    /*   41 */  CR( 8,  0,  F4, 0,  1,  0,  0),     /*  F4 (6 MSBs) */
                /* Mode 11 only             */
    /*   42 */  CR( 8,  0,  F5, 0,  1,  0,  0),     /*  F5 (6 MSBs) */

    /*   43 */  0,  /* unused */
    /*   44 */  0,  /* unused */

    /* -------------------------------------------------------------------- */
    /*  Opcode 1001: DELTA_9                                                */
    /* -------------------------------------------------------------------- */
                /* Mode 00 and 01           */
    /*   45 */  CR( 4,  2,  AM, 1,  0,  0,  0),     /*  Amplitude   */
    /*   46 */  CR( 5,  0,  PR, 1,  0,  0,  0),     /*  Period      */
    /*   47 */  CR( 3,  4,  B0, 1,  0,  0,  0),     /*  B0 4 MSBs   */
    /*   48 */  CR( 3,  3,  F0, 1,  0,  0,  0),     /*  F0 5 MSBs   */
    /*   49 */  CR( 3,  4,  B1, 1,  0,  0,  0),     /*  B1 4 MSBs   */
    /*   50 */  CR( 3,  3,  F1, 1,  0,  0,  0),     /*  F1 5 MSBs   */
    /*   51 */  CR( 3,  4,  B2, 1,  0,  0,  0),     /*  B2 4 MSBs   */
    /*   52 */  CR( 3,  3,  F2, 1,  0,  0,  0),     /*  F2 5 MSBs   */
    /*   53 */  CR( 3,  3,  B3, 1,  0,  0,  0),     /*  B3 5 MSBs   */
    /*   54 */  CR( 4,  2,  F3, 1,  0,  0,  0),     /*  F3 6 MSBs   */
    /*   55 */  CR( 4,  1,  B4, 1,  0,  0,  0),     /*  B4 7 MSBs   */
    /*   56 */  CR( 4,  2,  F4, 1,  0,  0,  0),     /*  F4 6 MSBs   */
                /* Mode 01 only             */
    /*   57 */  CR( 5,  0,  B5, 1,  0,  0,  0),     /*  B5 8 MSBs   */
    /*   58 */  CR( 5,  0,  F5, 1,  0,  0,  0),     /*  F5 8 MSBs   */

                /* Mode 10 and 11           */
    /*   59 */  CR( 4,  2,  AM, 1,  0,  0,  0),     /*  Amplitude   */
    /*   60 */  CR( 5,  0,  PR, 1,  0,  0,  0),     /*  Period      */
    /*   61 */  CR( 4,  1,  B0, 1,  0,  0,  0),     /*  B0 7 MSBs   */
    /*   62 */  CR( 4,  2,  F0, 1,  0,  0,  0),     /*  F0 6 MSBs   */
    /*   63 */  CR( 4,  1,  B1, 1,  0,  0,  0),     /*  B1 7 MSBs   */
    /*   64 */  CR( 4,  2,  F1, 1,  0,  0,  0),     /*  F1 6 MSBs   */
    /*   65 */  CR( 4,  1,  B2, 1,  0,  0,  0),     /*  B2 7 MSBs   */
    /*   66 */  CR( 4,  2,  F2, 1,  0,  0,  0),     /*  F2 6 MSBs   */
    /*   67 */  CR( 4,  1,  B3, 1,  0,  0,  0),     /*  B3 7 MSBs   */
    /*   68 */  CR( 5,  1,  F3, 1,  0,  0,  0),     /*  F3 7 MSBs   */
    /*   69 */  CR( 5,  0,  B4, 1,  0,  0,  0),     /*  B4 8 MSBs   */
    /*   70 */  CR( 5,  0,  F4, 1,  0,  0,  0),     /*  F4 8 MSBs   */
                /* Mode 11 only             */
    /*   71 */  CR( 5,  0,  B5, 1,  0,  0,  0),     /*  B5 8 MSBs   */
    /*   72 */  CR( 5,  0,  F5, 1,  0,  0,  0),     /*  F5 8 MSBs   */

    /* -------------------------------------------------------------------- */
    /*  Opcode 1010: SETMSB_A                                               */
    /* -------------------------------------------------------------------- */
                /* Mode 00 only             */
    /*   73 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 00 and 01           */
    /*   74 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*   75 */  CR( 5,  3,  F0, 0,  1,  0,  0),     /*  F0 (5 MSBs) */
    /*   76 */  CR( 5,  3,  F1, 0,  1,  0,  0),     /*  F1 (5 MSBs) */
    /*   77 */  CR( 5,  3,  F2, 0,  1,  0,  0),     /*  F2 (5 MSBs) */

                /* Mode 10 only             */
    /*   78 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 10 and 11           */
    /*   79 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*   80 */  CR( 6,  2,  F0, 0,  1,  0,  0),     /*  F0 (6 MSBs) */
    /*   81 */  CR( 6,  2,  F1, 0,  1,  0,  0),     /*  F1 (6 MSBs) */
    /*   82 */  CR( 6,  2,  F2, 0,  1,  0,  0),     /*  F2 (6 MSBs) */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0010: LOAD_2  Mode 00 and 10                                 */
    /*  Opcode 1100: LOAD_C  Mode 00 and 10                                 */
    /* -------------------------------------------------------------------- */
                /* LOAD_2, LOAD_C  Mode 00  */
    /*   83 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*   84 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*   85 */  CR( 3,  4,  B0, 0,  0,  0,  0),     /*  B0 (S=0)    */
    /*   86 */  CR( 5,  3,  F0, 0,  0,  0,  0),     /*  F0          */
    /*   87 */  CR( 3,  4,  B1, 0,  0,  0,  0),     /*  B1 (S=0)    */
    /*   88 */  CR( 5,  3,  F1, 0,  0,  0,  0),     /*  F1          */
    /*   89 */  CR( 3,  4,  B2, 0,  0,  0,  0),     /*  B2 (S=0)    */
    /*   90 */  CR( 5,  3,  F2, 0,  0,  0,  0),     /*  F2          */
    /*   91 */  CR( 4,  3,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*   92 */  CR( 6,  2,  F3, 0,  0,  0,  0),     /*  F3          */
    /*   93 */  CR( 7,  1,  B4, 0,  0,  0,  0),     /*  B4          */
    /*   94 */  CR( 6,  2,  F4, 0,  0,  0,  0),     /*  F4          */
                /* LOAD_2 only              */
    /*   95 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*   96 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */

                /* LOAD_2, LOAD_C  Mode 10  */
    /*   97 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*   98 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*   99 */  CR( 6,  1,  B0, 0,  0,  0,  0),     /*  B0 (S=0)    */
    /*  100 */  CR( 6,  2,  F0, 0,  0,  0,  0),     /*  F0          */
    /*  101 */  CR( 6,  1,  B1, 0,  0,  0,  0),     /*  B1 (S=0)    */
    /*  102 */  CR( 6,  2,  F1, 0,  0,  0,  0),     /*  F1          */
    /*  103 */  CR( 6,  1,  B2, 0,  0,  0,  0),     /*  B2 (S=0)    */
    /*  104 */  CR( 6,  2,  F2, 0,  0,  0,  0),     /*  F2          */
    /*  105 */  CR( 6,  1,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*  106 */  CR( 7,  1,  F3, 0,  0,  0,  0),     /*  F3          */
    /*  107 */  CR( 8,  0,  B4, 0,  0,  0,  0),     /*  B4          */
    /*  108 */  CR( 8,  0,  F4, 0,  0,  0,  0),     /*  F4          */
                /* LOAD_2 only              */
    /*  109 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*  110 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */

    /* -------------------------------------------------------------------- */
    /*  OPCODE 1101: DELTA_D                                                */
    /* -------------------------------------------------------------------- */
                /* Mode 00 and 01           */
    /*  111 */  CR( 4,  2,  AM, 1,  0,  0,  0),     /*  Amplitude   */
    /*  112 */  CR( 5,  0,  PR, 1,  0,  0,  0),     /*  Period      */
    /*  113 */  CR( 3,  3,  B3, 1,  0,  0,  0),     /*  B3 5 MSBs   */
    /*  114 */  CR( 4,  2,  F3, 1,  0,  0,  0),     /*  F3 6 MSBs   */
    /*  115 */  CR( 4,  1,  B4, 1,  0,  0,  0),     /*  B4 7 MSBs   */
    /*  116 */  CR( 4,  2,  F4, 1,  0,  0,  0),     /*  F4 6 MSBs   */
                /* Mode 01 only             */
    /*  117 */  CR( 5,  0,  B5, 1,  0,  0,  0),     /*  B5 8 MSBs   */
    /*  118 */  CR( 5,  0,  F5, 1,  0,  0,  0),     /*  F5 8 MSBs   */

                /* Mode 10 and 11           */
    /*  119 */  CR( 4,  2,  AM, 1,  0,  0,  0),     /*  Amplitude   */
    /*  120 */  CR( 5,  0,  PR, 1,  0,  0,  0),     /*  Period      */
    /*  121 */  CR( 4,  1,  B3, 1,  0,  0,  0),     /*  B3 7 MSBs   */
    /*  122 */  CR( 5,  1,  F3, 1,  0,  0,  0),     /*  F3 7 MSBs   */
    /*  123 */  CR( 5,  0,  B4, 1,  0,  0,  0),     /*  B4 8 MSBs   */
    /*  124 */  CR( 5,  0,  F4, 1,  0,  0,  0),     /*  F4 8 MSBs   */
                /* Mode 11 only             */
    /*  125 */  CR( 5,  0,  B5, 1,  0,  0,  0),     /*  B5 8 MSBs   */
    /*  126 */  CR( 5,  0,  F5, 1,  0,  0,  0),     /*  F5 8 MSBs   */

    /* -------------------------------------------------------------------- */
    /*  OPCODE 1110: LOAD_E                                                 */
    /* -------------------------------------------------------------------- */
    /*  127 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*  128 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0010: LOAD_2  Mode 01 and 11                                 */
    /*  Opcode 1100: LOAD_C  Mode 01 and 11                                 */
    /* -------------------------------------------------------------------- */
                /* LOAD_2, LOAD_C  Mode 01  */
    /*  129 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*  130 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*  131 */  CR( 3,  4,  B0, 0,  0,  0,  0),     /*  B0 (S=0)    */
    /*  132 */  CR( 5,  3,  F0, 0,  0,  0,  0),     /*  F0          */
    /*  133 */  CR( 3,  4,  B1, 0,  0,  0,  0),     /*  B1 (S=0)    */
    /*  134 */  CR( 5,  3,  F1, 0,  0,  0,  0),     /*  F1          */
    /*  135 */  CR( 3,  4,  B2, 0,  0,  0,  0),     /*  B2 (S=0)    */
    /*  136 */  CR( 5,  3,  F2, 0,  0,  0,  0),     /*  F2          */
    /*  137 */  CR( 4,  3,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*  138 */  CR( 6,  2,  F3, 0,  0,  0,  0),     /*  F3          */
    /*  139 */  CR( 7,  1,  B4, 0,  0,  0,  0),     /*  B4          */
    /*  140 */  CR( 6,  2,  F4, 0,  0,  0,  0),     /*  F4          */
    /*  141 */  CR( 8,  0,  B5, 0,  0,  0,  0),     /*  B5          */
    /*  142 */  CR( 8,  0,  F5, 0,  0,  0,  0),     /*  F5          */
                /* LOAD_2 only              */
    /*  143 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*  144 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */

                /* LOAD_2, LOAD_C  Mode 11  */
    /*  145 */  CR( 6,  2,  AM, 0,  0,  0,  1),     /*  Amplitude   */
    /*  146 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*  147 */  CR( 6,  1,  B0, 0,  0,  0,  0),     /*  B0 (S=0)    */
    /*  148 */  CR( 6,  2,  F0, 0,  0,  0,  0),     /*  F0          */
    /*  149 */  CR( 6,  1,  B1, 0,  0,  0,  0),     /*  B1 (S=0)    */
    /*  150 */  CR( 6,  2,  F1, 0,  0,  0,  0),     /*  F1          */
    /*  151 */  CR( 6,  1,  B2, 0,  0,  0,  0),     /*  B2 (S=0)    */
    /*  152 */  CR( 6,  2,  F2, 0,  0,  0,  0),     /*  F2          */
    /*  153 */  CR( 6,  1,  B3, 0,  0,  0,  0),     /*  B3 (S=0)    */
    /*  154 */  CR( 7,  1,  F3, 0,  0,  0,  0),     /*  F3          */
    /*  155 */  CR( 8,  0,  B4, 0,  0,  0,  0),     /*  B4          */
    /*  156 */  CR( 8,  0,  F4, 0,  0,  0,  0),     /*  F4          */
    /*  157 */  CR( 8,  0,  B5, 0,  0,  0,  0),     /*  B5          */
    /*  158 */  CR( 8,  0,  F5, 0,  0,  0,  0),     /*  F5          */
                /* LOAD_2 only              */
    /*  159 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*  160 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */

    /* -------------------------------------------------------------------- */
    /*  Opcode 0011: SETMSB_3                                               */
    /*  Opcode 0101: SETMSB_5                                               */
    /* -------------------------------------------------------------------- */
                /* Mode 00 only             */
    /*  161 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 00 and 01           */
    /*  162 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*  163 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*  164 */  CR( 5,  3,  F0, 0,  1,  0,  0),     /*  F0 (5 MSBs) */
    /*  165 */  CR( 5,  3,  F1, 0,  1,  0,  0),     /*  F1 (5 MSBs) */
    /*  166 */  CR( 5,  3,  F2, 0,  1,  0,  0),     /*  F2 (5 MSBs) */
                /* SETMSB_3 only            */
    /*  167 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*  168 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */

                /* Mode 10 only             */
    /*  169 */  CR( 0,  0,  0,  0,  0,  1,  0),     /*  Clear 5     */
                /* Mode 10 and 11           */
    /*  170 */  CR( 6,  2,  AM, 0,  0,  0,  0),     /*  Amplitude   */
    /*  171 */  CR( 8,  0,  PR, 0,  0,  0,  0),     /*  Period      */
    /*  172 */  CR( 6,  2,  F0, 0,  1,  0,  0),     /*  F0 (6 MSBs) */
    /*  173 */  CR( 6,  2,  F1, 0,  1,  0,  0),     /*  F1 (6 MSBs) */
    /*  174 */  CR( 6,  2,  F2, 0,  1,  0,  0),     /*  F2 (6 MSBs) */
                /* SETMSB_3 only            */
    /*  175 */  CR( 5,  0,  IA, 0,  0,  0,  0),     /*  Ampl. Intr. */
    /*  176 */  CR( 5,  0,  IP, 0,  0,  0,  0),     /*  Per. Intr.  */
};

static const SHORT sp0256_df_idx[16 * 8] =
{
    /*  OPCODE 0000 */      -1, -1,     -1, -1,     -1, -1,     -1, -1,
    /*  OPCODE 1000 */      -1, -1,     -1, -1,     -1, -1,     -1, -1,
    /*  OPCODE 0100 */      17, 22,     17, 24,     25, 30,     25, 32,
    /*  OPCODE 1100 */      83, 94,     129,142,    97, 108,    145,158,
    /*  OPCODE 0010 */      83, 96,     129,144,    97, 110,    145,160,
    /*  OPCODE 1010 */      73, 77,     74, 77,     78, 82,     79, 82,
    /*  OPCODE 0110 */      33, 36,     34, 37,     38, 41,     39, 42,
    /*  OPCODE 1110 */      127,128,    127,128,    127,128,    127,128,
    /*  OPCODE 0001 */      1,  14,     1,  16,     1,  14,     1,  16,
    /*  OPCODE 1001 */      45, 56,     45, 58,     59, 70,     59, 72,
    /*  OPCODE 0101 */      161,166,    162,166,    169,174,    170,174,
    /*  OPCODE 1101 */      111,116,    111,118,    119,124,    119,126,
    /*  OPCODE 0011 */      161,168,    162,168,    169,176,    170,176,
    /*  OPCODE 1011 */      -1, -1,     -1, -1,     -1, -1,     -1, -1,
    /*  OPCODE 0111 */      -1, -1,     -1, -1,     -1, -1,     -1, -1,
    /*  OPCODE 1111 */      0,  0,      0,  0,      0,  0,      0,  0
};

/* ======================================================================== */
/*  BITREV32       -- Bit-reverse a 32-bit number.                            */
/* ======================================================================== */
static ULONG bitrev32(ULONG val)
{
    val = ((val & 0xFFFF0000) >> 16) | ((val & 0x0000FFFF) << 16);
    val = ((val & 0xFF00FF00) >>  8) | ((val & 0x00FF00FF) <<  8);
    val = ((val & 0xF0F0F0F0) >>  4) | ((val & 0x0F0F0F0F) <<  4);
    val = ((val & 0xCCCCCCCC) >>  2) | ((val & 0x33333333) <<  2);
    val = ((val & 0xAAAAAAAA) >>  1) | ((val & 0x55555555) <<  1);

    return val;
}

/* ======================================================================== */
/*  BITREV8       -- Bit-reverse a 8-bit number.                            */
/* ======================================================================== */
static UBYTE bitrev8(UBYTE val)
{
	val = ((val & 0xF0) >>  4) | ((val & 0x0F) <<  4);
	val = ((val & 0xCC) >>  2) | ((val & 0x33) <<  2);
	val = ((val & 0xAA) >>  1) | ((val & 0x55) <<  1);

	return val;
}

/* ======================================================================== */
/*  BITREVBUFF       -- Bit-reverse a buffer.                               */
/* ======================================================================== */
static void bitrevbuff(const UBYTE *src, UBYTE *dst, unsigned int start, unsigned int length)
{
	for (unsigned int i = start; i < length; i++ )
		dst[i] = bitrev8(src[i]);
}

/* ======================================================================== */
/*  SP0256_GETB  -- Get up to 8 bits at the current PC.                     */
/* ======================================================================== */
static ULONG getb(struct sp0256_t *sp, int len)
{
    ULONG data = 0;
    ULONG d0, d1;

    /* -------------------------------------------------------------------- */
    /*  Fetch data from the FIFO or from the MASK                           */
    /* -------------------------------------------------------------------- */
    if (sp->fifo_sel)
    {
        d0 = sp->fifo[(sp->fifo_tail    ) & 63];
        d1 = sp->fifo[(sp->fifo_tail + 1) & 63];

        data = ((d1 << 10) | d0) >> sp->fifo_bitp;

        LOG_FIFO(("sp0256: RD_FIFO %.3X %d.%d %d\n", data & ((1 << len) - 1),
                sp->fifo_tail, sp->fifo_bitp, sp->fifo_head));

        /* ---------------------------------------------------------------- */
        /*  Note the PC doesn't advance when we execute from FIFO.          */
        /*  Just the FIFO's bit-pointer advances.   (That's not REALLY      */
        /*  what happens, but that's roughly how it behaves.)               */
        /* ---------------------------------------------------------------- */
        sp->fifo_bitp += len;
        if (sp->fifo_bitp >= 10)
        {
            sp->fifo_tail++;
            sp->fifo_bitp -= 10;
        }
    } else
    {
        /* ---------------------------------------------------------------- */
        /*  Figure out which ROMs are being fetched into, and grab two      */
        /*  adjacent bytes.  The byte we're interested in is extracted      */
        /*  from the appropriate bit-boundary between them.                 */
        /* ---------------------------------------------------------------- */
        int idx0 = (sp->pc    ) >> 3, d0;
        int idx1 = (sp->pc + 8) >> 3, d1;

        d0 = sp->rom[idx0 & (sp->size - 1)];
        d1 = sp->rom[idx1 & (sp->size - 1)];

        data = ((d1 << 8) | d0) >> (sp->pc & 7);

        sp->pc += len;
    }

    /* -------------------------------------------------------------------- */
    /*  Mask data to the requested length.                                  */
    /* -------------------------------------------------------------------- */
    data &= ((1 << len) - 1);

    return data;
}

/* ======================================================================== */
/*  SP0256_MICRO -- Emulate the microsequencer in the SP0256.  Executes     */
/*                  instructions either until the repeat count != 0 or      */
/*                  the sequencer gets halted by a RTS to 0.                */
/* ======================================================================== */
static VOID micro(struct sp0256_t *sp)
{
    UBYTE  immed4;
    UBYTE  opcode;
    USHORT cr;
    int ctrl_xfer = 0;
    int repeat    = 0;
    int i, idx0, idx1;

    /* -------------------------------------------------------------------- */
    /*  Only execute instructions while the filter is not busy.             */
    /* -------------------------------------------------------------------- */
    while (sp->filt.rpt <= 0)
    {
        /* ---------------------------------------------------------------- */
        /*  If the CPU is halted, see if we have a new command pending      */
        /*  in the Address LoaD buffer.                                     */
        /* ---------------------------------------------------------------- */
        if (sp->halted && !sp->lrq)
        {
            sp->pc       = sp->ald;
            sp->fifo_sel = 0;
            sp->halted   = 0;
            sp->lrq      = 0x8000;
            sp->ald      = 0;
            for (i = 0; i < 16; i++)
                sp->filt.r[i] = 0;
        }

        /* ---------------------------------------------------------------- */
        /*  If we're still halted, do nothing.                              */
        /* ---------------------------------------------------------------- */
        if (sp->halted)
        {
            return;
        }

        /* ---------------------------------------------------------------- */
        /*  Fetch the first 8 bits of the opcode, which are always in the   */
        /*  same approximate format -- immed4 followed by opcode.           */
        /* ---------------------------------------------------------------- */
        immed4 = getb(sp, 4);
        opcode = getb(sp, 4);
        repeat = 0;
        ctrl_xfer = 0;

        LOG(("$%.4X.%.1X: OPCODE %d%d%d%d.%d%d\n",
                (sp->pc >> 3) - 1, sp->pc & 7,
                !!(opcode & 1), !!(opcode & 2),
                !!(opcode & 4), !!(opcode & 8),
                !!(sp->mode&4), !!(sp->mode&2)));

        /* ---------------------------------------------------------------- */
        /*  Handle the special cases for specific opcodes.                  */
        /* ---------------------------------------------------------------- */
        switch (opcode)
        {
            /* ------------------------------------------------------------ */
            /*  OPCODE 0000:  RTS / SETPAGE                                 */
            /* ------------------------------------------------------------ */
            case 0x0:
            {
                /* -------------------------------------------------------- */
                /*  If immed4 != 0, then this is a SETPAGE instruction.     */
                /* -------------------------------------------------------- */
                if (immed4)     /* SETPAGE */
                {
                    sp->page = bitrev32(immed4) >> 13;
                } else
                /* -------------------------------------------------------- */
                /*  Otherwise, this is an RTS / HLT.                        */
                /* -------------------------------------------------------- */
                {
                    ULONG btrg;

                    /* ---------------------------------------------------- */
                    /*  Figure out our branch target.                       */
                    /* ---------------------------------------------------- */
                    btrg = sp->stack;

                    sp->stack = 0;

                    /* ---------------------------------------------------- */
                    /*  If the branch target is zero, this is a HLT.        */
                    /*  Otherwise, it's an RTS, so set the PC.              */
                    /* ---------------------------------------------------- */
                    if (!btrg)
                    {
                        sp->halted   = 1;
                        sp->sby_line = 1;

                        sp->pc       = 0;
                        ctrl_xfer    = 1;
                    } else
                    {
                        sp->pc      = btrg;
                        ctrl_xfer   = 1;
                    }
                }

                break;
            }

            /* ------------------------------------------------------------ */
            /*  OPCODE 0111:  JMP          Jump to 12-bit/16-bit Abs Addr   */
            /*  OPCODE 1011:  JSR          Jump to Subroutine               */
            /* ------------------------------------------------------------ */
            case 0xE:
            case 0xD:
            {
                int btrg;

                /* -------------------------------------------------------- */
                /*  Figure out our branch target.                           */
                /* -------------------------------------------------------- */
                btrg = sp->page                   |
                        (bitrev32(immed4)  >> 17) |
                        (bitrev32(getb(sp, 8)) >> 21);
                ctrl_xfer = 1;

                /* -------------------------------------------------------- */
                /*  If this is a JSR, push our return address on the        */
                /*  stack.  Make sure it's byte aligned.                    */
                /* -------------------------------------------------------- */
                if (opcode == 0xD)
                    sp->stack = (sp->pc + 7) & ~7;

                /* -------------------------------------------------------- */
                /*  Jump to the new location!                               */
                /* -------------------------------------------------------- */
                sp->pc = btrg;
                break;
            }

            /* ------------------------------------------------------------ */
            /*  OPCODE 1000:  SETMODE      Set the Mode and Repeat MSBs     */
            /* ------------------------------------------------------------ */
            case 0x1:
            {
                sp->mode = ((immed4 & 8) >> 2) | (immed4 & 4) | ((immed4 & 3) << 4);
                break;
            }

            /* ------------------------------------------------------------ */
            /*  OPCODE 0001:  LOADALL      Load All Parameters              */
            /*  OPCODE 0010:  LOAD_2       Load Per, Ampl, Coefs, Interp.   */
            /*  OPCODE 0011:  SETMSB_3     Load Pitch, Ampl, MSBs, & Intrp  */
            /*  OPCODE 0100:  LOAD_4       Load Pitch, Ampl, Coeffs         */
            /*  OPCODE 0101:  SETMSB_5     Load Pitch, Ampl, and Coeff MSBs */
            /*  OPCODE 0110:  SETMSB_6     Load Ampl, and Coeff MSBs.       */
            /*  OPCODE 1001:  DELTA_9      Delta update Ampl, Pitch, Coeffs */
            /*  OPCODE 1010:  SETMSB_A     Load Ampl and MSBs of 3 Coeffs   */
            /*  OPCODE 1100:  LOAD_C       Load Pitch, Ampl, Coeffs         */
            /*  OPCODE 1101:  DELTA_D      Delta update Ampl, Pitch, Coeffs */
            /*  OPCODE 1110:  LOAD_E       Load Pitch, Amplitude            */
            /*  OPCODE 1111:  PAUSE        Silent pause                     */
            /* ------------------------------------------------------------ */
            default:
            {
                repeat = immed4 | (sp->mode & 0x30);
                break;
            }
        }
        if (opcode != 1) sp->mode &= 0xF;

        /* ---------------------------------------------------------------- */
        /*  If this was a control transfer, handle setting "fifo_sel"       */
        /*  and all that ugliness.                                          */
        /* ---------------------------------------------------------------- */
        if (ctrl_xfer)
        {
            LOG(("jumping to $%.4X.%.1X: ", sp->pc >> 3, sp->pc & 7));

            /* ------------------------------------------------------------ */
            /*  Set our "FIFO Selected" flag based on whether we're going   */
            /*  to the FIFO's address.                                      */
            /* ------------------------------------------------------------ */
            sp->fifo_sel = sp->pc == FIFO_ADDR;

            LOG(("%s ", sp->fifo_sel ? "FIFO" : "ROM"));

            /* ------------------------------------------------------------ */
            /*  Control transfers to the FIFO cause it to discard the       */
            /*  partial decle that's at the front of the FIFO.              */
            /* ------------------------------------------------------------ */
            if (sp->fifo_sel && sp->fifo_bitp)
            {
                LOG(("bitp = %d -> Flush", sp->fifo_bitp));

                /* Discard partially-read decle. */
                if (sp->fifo_tail < sp->fifo_head) sp->fifo_tail++;
                sp->fifo_bitp = 0;
            }

            LOG(("\n"));

            continue;
        }

        /* ---------------------------------------------------------------- */
        /*  Otherwise, if we have a repeat count, then go grab the data     */
        /*  block and feed it to the filter.                                */
        /* ---------------------------------------------------------------- */
        if (!repeat) continue;

        sp->filt.rpt = repeat + 1;
        LOG(("repeat = %d\n", repeat));

        i = (opcode << 3) | (sp->mode & 6);
        idx0 = sp0256_df_idx[i++];
        idx1 = sp0256_df_idx[i  ];

        //assert(idx0 >= 0 && idx1 >= 0 && idx1 >= idx0);

        /* ---------------------------------------------------------------- */
        /*  Step through control words in the description for data block.   */
        /* ---------------------------------------------------------------- */
        for (i = idx0; i <= idx1; i++)
        {
            int len, shf, delta, field, prm, clra, clr5;
            BYTE value;

            /* ------------------------------------------------------------ */
            /*  Get the control word and pull out some important fields.    */
            /* ------------------------------------------------------------ */
            cr = sp0256_datafmt[i];

            len   = CR_LEN(cr);
            shf   = CR_SHF(cr);
            prm   = CR_PRM(cr);
            clra  = cr & CR_CLRA;
            clr5  = cr & CR_CLR5;
            delta = cr & CR_DELTA;
            field = cr & CR_FIELD;
            value = 0;

            LOG(("$%.4X.%.1X: len=%2d shf=%2d prm=%2d d=%d f=%d ",
                        sp->pc >> 3, sp->pc & 7, len, shf, prm, !!delta, !!field));
            /* ------------------------------------------------------------ */
            /*  Clear any registers that were requested to be cleared.      */
            /* ------------------------------------------------------------ */
            if (clra)
            {
                for (int j = 0; j < 16; j++)
                    sp->filt.r[j] = 0;

                sp->silent = 1;
            }

            if (clr5)
                sp->filt.r[B5] = sp->filt.r[F5] = 0;

            /* ------------------------------------------------------------ */
            /*  If this entry has a bitfield with it, grab the bitfield.    */
            /* ------------------------------------------------------------ */
            if (len)
            {
                value = getb(sp, len);
            }
            else
            {
                LOG((" (no update)\n"));
                continue;
            }

            /* ------------------------------------------------------------ */
            /*  Sign extend if this is a delta update.                      */
            /* ------------------------------------------------------------ */
            if (delta)  /* Sign extend */
            {
                if (value & (1 << (len - 1))) value |= -1 << len;
            }

            /* ------------------------------------------------------------ */
            /*  Shift the value to the appropriate precision.               */
            /* ------------------------------------------------------------ */
            if (shf)
                value <<= shf;

            LOG(("v=%.2X (%c%.2X)  ", value & 0xFF,
                        value & 0x80 ? '-' : '+',
                        0xFF & (value & 0x80 ? -value : value)));

            sp->silent = 0;

            /* ------------------------------------------------------------ */
            /*  If this is a field-replace, insert the field.               */
            /* ------------------------------------------------------------ */
            if (field)
            {
                LOG(("--field-> r[%2d] = %.2X -> ", prm, sp->filt.r[prm]));

                sp->filt.r[prm] &= ~(~0 << shf); /* Clear the old bits.     */
                sp->filt.r[prm] |= value;        /* Merge in the new bits.  */

                LOG(("%.2X\n", sp->filt.r[prm]));

                continue;
            }

            /* ------------------------------------------------------------ */
            /*  If this is a delta update, add to the appropriate field.    */
            /* ------------------------------------------------------------ */
            if (delta)
            {
                LOG(("--delta-> r[%2d] = %.2X -> ", prm, sp->filt.r[prm]));

                sp->filt.r[prm] += value;

                LOG(("%.2X\n", sp->filt.r[prm]));

                continue;
            }

            /* ------------------------------------------------------------ */
            /*  Otherwise, just write the new value.                        */
            /* ------------------------------------------------------------ */
            sp->filt.r[prm] = value;
            LOG(("--value-> r[%2d] = %.2X\n", prm, sp->filt.r[prm]));
        }

        /* ---------------------------------------------------------------- */
        /*  Special case:  Set PAUSE's equivalent period.                   */
        /* ---------------------------------------------------------------- */
        if (opcode == 0xF)
        {
            sp->silent = 1;
            sp->filt.r[1] = PER_PAUSE;
        }

        /* ---------------------------------------------------------------- */
        /*  Now that we've updated the registers, go decode them.           */
        /* ---------------------------------------------------------------- */
        lpc12_regdec(&sp->filt);

        /* ---------------------------------------------------------------- */
        /*  Break out since we now have a repeat count.                     */
        /* ---------------------------------------------------------------- */
        break;
    }
}

