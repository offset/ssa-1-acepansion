/*
** Converted to plain C without MESS dependencies
** by Philippe 'OffseT' Rimauro
*/

/**********************************************************************

    SP0256 Narrator Speech Processor emulation

    Copyright MESS Team.
    Visit http://mamedev.org for licensing and usage restrictions.

**********************************************************************
                            _____   _____
                   Vss   1 |*    \_/     | 28  OSC 2
                _RESET   2 |             | 27  OSC 1
           ROM DISABLE   3 |             | 26  ROM CLOCK
                    C1   4 |             | 25  _SBY RESET
                    C2   5 |             | 24  DIGITAL OUT
                    C3   6 |             | 23  Vdi
                   Vdd   7 |    SP0256   | 22  TEST
                   SBY   8 |             | 21  SER IN
                  _LRQ   9 |             | 20  _ALD
                    A8  10 |             | 19  SE
                    A7  11 |             | 18  A1
               SER OUT  12 |             | 17  A2
                    A6  13 |             | 16  A3
                    A5  14 |_____________| 15  A4

**********************************************************************/

/*
   GI SP0256 Narrator Speech Processor

   By Joe Zbiciak. Ported to MESS by tim lindner.

 Copyright Joseph Zbiciak, all rights reserved.
 Copyright tim lindner, all rights reserved.

 - This source code is released as freeware for non-commercial purposes.
 - You are free to use and redistribute this code in modified or
   unmodified form, provided you list us in the credits.
 - If you modify this source code, you must add a notice to each
   modified source file that it has been changed.  If you're a nice
   person, you will clearly mark each change too.  :)
 - If you wish to use this for commercial purposes, please contact us at
   intvnut@gmail.com (Joe Zbiciak), tlindner@macmess.org (tim lindner)
 - This entire notice must remain in the source code.

*/

#ifndef __SP0256_H__
#define __SP0256_H__


#ifndef EXEC_TYPES_H
#include <exec/types.h>
#endif


/* ---------------------------------------------------------------- */
/*  Default ROM tags for sp0256_create (size is then ignored).      */
/* ---------------------------------------------------------------- */
#define SP0256_ROM_AL2 ((const UBYTE *)-1)


/* ---------------------------------------------------------------- */
/*  Actual public API.                                              */
/* ---------------------------------------------------------------- */
struct sp0256_t;

struct sp0256_t * sp0256_create(int clock_hz, const UBYTE *rom, USHORT size);
VOID sp0256_destroy(struct sp0256_t *sp);

VOID sp0256_update_clock(struct sp0256_t *sp, int clock_hz);

VOID sp0256_tick(struct sp0256_t *sp);
VOID sp0256_reset(struct sp0256_t *sp);
SHORT sp0256_get_audio(struct sp0256_t *sp);

VOID sp0256_write_ald(struct sp0256_t *sp, UBYTE data);

UBYTE sp0256_read_lrq(struct sp0256_t *sp);
UBYTE sp0256_read_sby(struct sp0256_t *sp);

VOID sp0256_write_spb640_ald(struct sp0256_t *sp, USHORT data);
VOID sp0256_write_spb640_fifo(struct sp0256_t *sp, USHORT data);

USHORT sp0256_read_spb640_lrq(struct sp0256_t *sp);
USHORT sp0256_read_spb640_fifo(struct sp0256_t *sp);


#endif /* __SP0256_H__ */
