## Version $VER: ssa-1.acepansion.catalog 1.0 (11.04.2021)
## Languages english fran�ais deutsch espa�ol
## Codeset english 0
## Codeset fran�ais 0
## Codeset deutsch 0
## Codeset espa�ol 0
## SimpleCatConfig CharsPerLine 200
## Header Locale_Strings
## TARGET C english "generated/locale_strings.h" NoCode
## TARGET CATALOG fran�ais "Release/Catalogs/fran�ais/" Optimize
## TARGET CATALOG deutsch "Release/Catalogs/deutsch/" Optimize
## TARGET CATALOG espa�ol "Release/Catalogs/espa�ol/" Optimize
MSG_MENU_TOGGLE
Plug a SSA-1 speech synthesizer
Brancher un synth�tiseur vocal SSA-1
SSA-1 Sprachsynthesizer anschlie�en
Conecte un sintetizador de voz SSA-1
;
MSG_TITLE
SSA-1 speech synthesizer
Synth�tiseur vocal SSA-1
SSA-1 Sprachsynthesizer
Sintetizador de voz SSA-1
;
